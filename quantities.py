import sys

items = {}

def op_add():
    item = sys.argv[0]
    quantity = int(sys.argv[1])
    adlist = {item:quantity}
    items.update(adlist)
    return items


def op_items():
    for k in items.keys():
        print(k, end=" ")

def op_all():
    for i in items.items():
        print(i)

def op_sum():
    sulist = sum(items.values())
    print(sulist)

def main():
    while sys.argv != 0:
        op = sys.argv.pop(0)
        if op == "add":
            op_add()
        elif op == "items":
            op_items()
        elif op == "sum":
            op_sum()
        elif op == "all":
            op_all()


if __name__ == '__main__':
    main()
